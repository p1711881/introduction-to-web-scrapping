# Introduction to Web Scrapping

## Description :

Ce projet est une introduction au web scrapping sous R, qui utilisent les bibliothèques rvest et purr.

Cette introduction présente la manière dont on peut récupérer des informations via un url donné.

Nous ferons l'expérimentation sur le site [senscritique.com](https://www.senscritique.com/films).

Nous créerons notre propre jeu de données contenant des films et les différents avis ou notes sur ces films.

## Une version automatisée utilisant RSelenium sera disponible dans le futur.
